package latihan;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class frameKu_latihan4 extends JFrame {

    public frameKu_latihan4() {
        this.setLayout(null);
        this.setSize(300, 100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);
        JLabel label = new JLabel("Keyword");
        label.setBounds(150, 20, 100, 20);
        this.add(label);
        JTextField text = new JTextField();
        text.setBounds(150, 50, 40, 20);
        this.add(text);
    }

    public static void main(String[] args) {
        new frameKu_latihan4();

    }
}
