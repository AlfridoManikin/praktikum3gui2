/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

/**
 *
 * @author TOSHIBA
 */
public class FrameKu_Latihan3 extends JFrame {

    public FrameKu_Latihan3() {
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);

        JPanel panel = new JPanel();
        JButton tombol = new JButton();
        tombol.setText("Ini Tombol");
        panel.add(tombol);
        this.add(panel);
        
        JLabel label = new JLabel();
        panel.add(label);
        this.add(panel);
        
        JTextField text = new JTextField();
        panel.add(text);
        this.add(panel);
        
        JCheckBox check = new JCheckBox();
        panel.add(check);
        this.add(panel);
        
        JRadioButton radio = new JRadioButton();
        panel.add(radio);
        this.add(panel);
    }

    public static void main(String[] args) {
        new FrameKu_Latihan3();
    }
}
