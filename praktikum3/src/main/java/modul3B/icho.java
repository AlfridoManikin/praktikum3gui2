/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modul3B;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class icho extends JFrame {
    private static final int FRAME_WIDTH = 350;
    private static final int FRAME_HEIGHT = 300;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 100;
    private static final int BUTTON_HEIGHT = 60;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txtField;
    
    public static void main(String[] args){
        icho frame =  new icho ();
        frame.setVisible(true);
    }   
   public icho (){
        Container contentPane = getContentPane();
        setSize ( FRAME_WIDTH, FRAME_HEIGHT );
        setResizable(true);
        setTitle("Input Data");
        setLocation( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        contentPane.setLayout(null);
        contentPane.setBackground(Color.white);
        
        JLabel label1 = new JLabel("Jenis Kelamin :");
        label1.setBounds(15, 30, 100, 21);
        contentPane.add(label1);
        
        JLabel label = new JLabel("Nama :");
        label.setBounds(15, 2, 40, 21);
        contentPane.add(label);
        
        JLabel label2 = new JLabel("Hobi :");
        label2.setBounds(15, 60, 40, 21);
        contentPane.add(label2);
        
        JTextField text = new JTextField(null);
        text.setBounds(103, 2, 150, 21);
        contentPane.add(text);
        
        JRadioButton radio = new JRadioButton("Laki-Laki");
        radio.setBounds(103, 30, 100, 21);
        contentPane.add(radio);
        
        JRadioButton radio2 = new JRadioButton("Perempuan");
        radio2.setBounds(203, 30, 100, 21);
        contentPane.add(radio2);
        
        JCheckBox box = new JCheckBox("Olahraga");
        box.setBounds(102, 60, 100, 21);
        contentPane.add(box);
        
        JCheckBox box1 = new JCheckBox("Shopping");
        box1.setBounds(102, 85, 100, 21);
        contentPane.add(box1);
        
        JCheckBox box2 = new JCheckBox("Computer Games");
        box2.setBounds(102, 110, 150, 21);
        contentPane.add(box2);
        
        JCheckBox box3 = new JCheckBox("Nonton Bioskop");
        box3.setBounds(102, 135, 150, 21);
        contentPane.add(box3);
        
        okButton = new JButton("Ok");
        okButton.setBounds(50, 190, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        cancelButton = new JButton("Cancel");
        cancelButton.setBounds(210, 190, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
} 
