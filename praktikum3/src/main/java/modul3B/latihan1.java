
package modul3B;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;



class Ch14AbsolutePositioning extends JFrame{
    private static final int FRAME_WIDHT = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDHT = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txtField;

public static void main(String[] args) {
    Ch14AbsolutePositioning frame = new Ch14AbsolutePositioning();
    frame.setVisible(true);
}
public Ch14AbsolutePositioning(){
    Container contentPane = getContentPane();
    
    setSize ( FRAME_WIDHT, FRAME_HEIGHT );
    setResizable( true );
    setTitle( "Program Ch14AbsolutePositioning" );
    setLocation ( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
    
    contentPane.setLayout( null);
    contentPane.setBackground( Color.white );
    okButton = new JButton ("OK");
    okButton.setBounds(200, 500, BUTTON_WIDHT, BUTTON_HEIGHT);
    contentPane.add(okButton);
    
    cancelButton = new JButton("CANCEL");
    cancelButton.setBounds(200, 400, BUTTON_WIDHT, BUTTON_HEIGHT);
    contentPane.add(cancelButton);
    
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}